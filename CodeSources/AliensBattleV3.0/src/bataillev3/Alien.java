package bataillev3;


public class Alien {

	
	public int longueur=30;
	public int hauteur=30;
	

	public int coordx;
	public int coordy;
	public int pointdevie=10;
	private double speed=1;
	//private boolean intel;

	public Alien(int x){
		this.coordx=x;
		this.coordy=hauteur/2-200;//en dehors de l'ecran pour eviter les aliens invincibles

	}
	
	public int getLongueur() {
		return longueur;
	}

	public int getHauteur() {
		return hauteur;
	}

	public int getCoordy() {
		return coordy;
	}

	public void setCoordy(int coordy) {
		this.coordy = coordy;
	}

	public void trajectoireintel(int coordshipx){
		double rd=Math.random();//probabilit� de d�placement intelligent (selon x)
		if(rd>0.75){
			coordx=coordx+5*(int)(Math.signum(coordshipx-this.coordx));
		}
		if(rd<0.5){
			coordy=coordy+3;
		}
		coordy=coordy+5;

	}

//	public void trajectoire(boolean a){
//		if(a==true){
//
//		}
//		else{
//			coordy=coordy+10;
//		}
//	}

	public int getCoordx() {
		return coordx;
	}

	public void setCoordx(int coordx) {
		this.coordx = coordx;
	}

	public void descendre(int dy){
		coordy=coordy-dy;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

}
