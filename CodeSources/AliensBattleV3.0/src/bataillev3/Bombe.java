package bataillev3;


public class Bombe {
	
	public int taille=8;
	public int coordx,coordy,ciblex,cibley,originex,originey;
	public int dx,dy;
	public double speed=1;
	
	public Bombe(int ox,int oy,int cx,int cy){
		coordx=ox;
		coordy=oy;
		originex=ox;
		originey=oy;
		ciblex=cx;
		cibley=cy;
		dx=cx-ox;
		dy=cy-oy;
	}
	
	public void mouvement(){
		coordx=coordx+(int)((dx/40)*speed);
		coordy=coordy+(int)((dy/40)*speed);
	}

	public int getCoordx() {
		return coordx;
	}

	public void setCoordx(int coordx) {
		this.coordx = coordx;
	}

	public int getCoordy() {
		return coordy;
	}

	public void setCoordy(int coordy) {
		this.coordy = coordy;
	}

	public int getDx() {
		return dx;
	}

	public void setDx(int dx) {
		this.dx = dx;
	}

	public int getDy() {
		return dy;
	}

	public void setDy(int dy) {
		this.dy = dy;
	}
	
	

}
