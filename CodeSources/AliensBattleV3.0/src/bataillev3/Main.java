package bataillev3;



import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.*;

/**
 * @author acl2016_ensem3
 * @since  2016-11-10
 */
public class Main {

	/**
	 */
	/**
	 * @param args 
	 */
	public static void main(String[] args) {
		// Dimension du jeu
		Dimension dimensionfen =new Dimension(Dessins.Xmax,Dessins.Ymax);

		//Parametrisation de la fenetre de jeu
		JFrame Fenetre= new JFrame();
		String title="AliensBattle";// Le titre
		Fenetre.setTitle(title);
		Fenetre.setSize(dimensionfen);
		Fenetre.setResizable(false);// Pour empecher d'agrandir l'�cran du jeu
		Fenetre.setLocationRelativeTo(null);//Pour le placer au milieu de l'�cran
		Component grosdessin;
		grosdessin = new Dessins();
		grosdessin.repaint();
		grosdessin.setForeground(Color.BLUE);
		grosdessin.setPreferredSize(dimensionfen);
		WindowAdapter wa= new WindowAdapter() {
			/**
			 * 
			 * @param e 
			 */
			@Override
			public void windowClosing(WindowEvent e){
				System.exit(0);}};

				Fenetre.addWindowListener(wa);
				Fenetre.getContentPane().add(grosdessin);
				Fenetre.pack();
				Fenetre.setVisible(true);


	}

}