package bataillev3;


import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.TimerTask;
import java.util.Vector;
import java.util.Timer;

/**
 * @author acl2016_ensem3
 * @since  2016-11-10
 */

public class Dessins extends JPanel {

	//On fixe la taille de la fenetre du jeu avec ces param�tres
	public static int Xmax=600, Ymax=650;
	private int score;
	private int niveau;
	public Vaisseau Ship=new Vaisseau(Xmax/2,Ymax-100);
	public Vector<Alien> listaliens=new Vector<Alien>();
	public Vector<Bombe> listebombes=new Vector<Bombe>();
	public Vector<Tir> listetir=new Vector<Tir>();
	public Timer timertirs;
	public Timer timerappaliens;
	public Timer timerdeplaliens;
	private Timer timerniveau;
	public Vector<Bonus> listebonus=new Vector();
	
	public Dessins(){
		this.setFocusable(true);
		this.setBackground(Color.lightGray);

		this.addKeyListener(new KeyListener()
				{   public void keyPressed(KeyEvent e) {


					int keyCode = e.getKeyCode();
					switch( keyCode ) {


					/**Touche flech�e gauche, on deplace le vaisseau vers la gauche jusqu'� une certaine distance
					 * de la bordure o� o� le vaisseau n'est plus visible dans l'interface GUI
					 */
					case KeyEvent.VK_LEFT:

						if(Ship.getCoordx()>20&&niveau!=(-1)){
							Ship.deplacerhorizontal(-10);
						}

						repaint();
						break;
						/**Touche flech�e droite, on deplace le vaisseau vers la droite jusqu'� une certaine distance 
						 *de la bordure o� le vaisseau n'est plus visible dans l'interface GUI
						 */
					case KeyEvent.VK_RIGHT :

						if(Ship.getCoordx()<Xmax-20&&niveau!=(-1)){
							Ship.deplacerhorizontal(10);
						}
						repaint();
						break;


					case KeyEvent.VK_A :
						if(niveau!=(-1)){
							listetir.add(new Tir(Ship.getCoordx()-20,530,-1));
						}
						repaint();
						break;

					case KeyEvent.VK_Z :
						if(niveau!=(-1)){
							listetir.add(new Tir(Ship.getCoordx(),520,0));
						}
						repaint();
						break;

					case KeyEvent.VK_E :
						if(niveau!=(-1)){
							listetir.add(new Tir(Ship.getCoordx()+20,530,1));
						}
						repaint();
						break;
					}

				}
	
				public void keyReleased(KeyEvent e) {}
				public void keyTyped(KeyEvent e) {}
				});


		timerniveau = new Timer();		//ce timer permet de g�rer le niveau de difficult� (10secondes = +1 niv)
		timerniveau.scheduleAtFixedRate(
				new TimerTask()
				{
					public void run(){
						if(niveau<25&&niveau>=0){
							niveau++;
						}
					}
				},0,20000);


		timertirs = new Timer(); //timer qui permet de g�rer le mouvement des tirs
		timertirs.scheduleAtFixedRate(
				new TimerTask() 
				{	public void run()  { 
					actionperiodique1();
					try {
						actionperiodique4();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
				}, 
				0, 50);

		timerappaliens = new Timer();  //timer qui permet de g�rer l'apparition des aliens
		timerappaliens.scheduleAtFixedRate(
				new TimerTask() 
				{	public void run()  { 
					actionperiodique2();
				}
				}, 

				0, 2500);

		timerdeplaliens = new Timer(); //timer qui permet de g�rer le deplacement des aliens
		timerdeplaliens.scheduleAtFixedRate(
				new TimerTask() 
				{	public void run()  { 
					actionperiodique5();
					try {
						actionperiodique3();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
				}, 
				200, 170);


	}

	public void actionperiodique1(){	//c'est ici que l'on genere le mouvement des tirs
		for(int i=0;i<this.listetir.size();i++){
			this.listetir.get(i).mouvement();
			if(listetir.get(i).getCoordy()<0){ //on enleve les tirs qui sont sortis de l'�cran
				listetir.remove(i);
			}
		}

		repaint();
		for(int i=0;i<this.listaliens.size()-this.niveau;i++){	//pour d�terminer la collision ou non d'un alien avec un tir
			for(int j=0;j<this.listetir.size();j++){
				int deltax=Math.abs(this.listaliens.get(i).getCoordx()-this.listetir.get(j).getCoordx());//diff�rence d'abscisse entre le vaisseau et un tir
				int deltay=Math.abs(this.listaliens.get(i).getCoordy()-this.listetir.get(j).getCoordy());//diff�rence d'ordonn�e entre le vaisseau et un tir
				int dist=(int) Math.sqrt((deltax*deltax)+(deltay*deltay));	//distance entre un tir et le vaisseau
				if(dist<=this.listaliens.get(i).getLongueur()/2){  	//cas o� un tir touche un alien
					this.score+=1;
					double probbonus=Math.random();
					if(probbonus>0.8+niveau*0.01){
						this.listebonus.addElement(new Bonus(this.listaliens.get(i).getCoordx(),this.listaliens.get(i).getCoordy()));
					}
					this.listaliens.remove(i);
					this.listetir.remove(j);
				}
			}
		}

	}

	public void actionperiodique2(){	//c'est ici que l'on genere l'apparition des aliens
		if(this.niveau>-1){
			int nbapp=(int)(Math.random()*(this.niveau))+1;
			for(int i=0;i<nbapp;i++){
				Alien a=new Alien(0);
				int r=(int)(Math.random()*(Xmax-a.longueur/2));
				if(r<a.longueur/2){
					r=a.longueur/2;
				}
				this.listaliens.add(new Alien(r));
			}
			repaint();
		}
	}

	public void actionperiodique3() throws InterruptedException{	//deplacement des aliens (+ cr�ation des bombes)
		for(int i=0;i<this.listaliens.size();i++){
			//			this.listaliens.get(i).trajectoire(false);
			this.listaliens.get(i).trajectoireintel(this.Ship.coordx);
			if(listaliens.get(i).getCoordy()>Ymax+20){
				listaliens.remove(i);
			}
		}

		for(int i=0;i<this.listaliens.size();i++){	//pour d�terminer la collision ou non d'un alien avec le vaisseau

			int deltax=Math.abs(this.listaliens.get(i).getCoordx()-this.Ship.getCoordx());//diff�rence d'abscisse entre le vaisseau et l'alien
			int deltay=Math.abs(this.listaliens.get(i).getCoordy()-this.Ship.getCoordy());//diff�rence d'ordonn�e entre le vaisseau et l'alien
			int dist=(int) Math.sqrt((deltax*deltax)+(deltay*deltay));//distance entre un alien et le vaisseau
			if(dist<Ship.longueur/2+listaliens.get(i).longueur/2){ //cas o� la collision a lieu
				this.Ship.setPointdevie(0);
				this.niveau=-1;
				this.listetir.removeAllElements();
				this.listebombes.removeAllElements();

			}
		}
		double rdbombe=Math.random(); //probabilit� de lancement de bombe d'un alien
		if(rdbombe>0.85){
			int rd=(int) (Math.random()*(listaliens.size()-1)); //choix al�atoire de l'alien qui va lancer la bombe
			if(listaliens.get(rd).getCoordy()>0){
				listebombes.add(new Bombe(this.listaliens.get(rd).getCoordx(),this.listaliens.get(rd).getCoordy(),this.Ship.getCoordx(),this.Ship.getCoordy()));
			}
		}
		repaint();
	}

	
	public void actionperiodique4() throws InterruptedException{	//c'est ici que l'on genere le mouvement des bombes
		for(int i=0;i<this.listebombes.size();i++){
			this.listebombes.get(i).mouvement();
			if(this.listebombes.get(i).getCoordy()>Ymax+20 || this.listebombes.get(i).getCoordy()<0){//on enleve les bombes qui sont sorties de l'�cran
				this.listebombes.remove(i);
			}
		}
		repaint();
		for(int i=0;i<this.listebombes.size()-1;i++){	//pour d�terminer la collision ou non d'une bombe avec le vaisseau

			int deltax=Math.abs(this.listebombes.get(i).getCoordx()-this.Ship.getCoordx());//diff�rence d'abscisse entre le vaisseau et une bombe
			int deltay=Math.abs(this.listebombes.get(i).getCoordy()-this.Ship.getCoordy());//diff�rence d'ordonn�e entre le vaisseau et une bombe
			int dist=(int) Math.sqrt((deltax*deltax)+(deltay*deltay));//distance entre une bombe et le vaisseau
			if(dist<=Ship.longueur/2+this.listebombes.get(i).taille/2){ //cas o� la collision � lieu
				this.Ship.degats(1);   // on retire un point de vie au vaisseau
				this.listebombes.remove(i);
			}
			if(this.Ship.getPointdevie()<=0){ // cas o� le vaisseau n'a plus de point de vie
				this.niveau=-1;
				this.listetir.removeAllElements();
				this.listebombes.removeAllElements();
			}
		}

	}

	public void actionperiodique5(){   // c'est ici que l'on genere le mouvement des bonus et la collision des bonus
		for(int i=0;i<listebonus.size();i++){
			this.listebonus.get(i).trajectoire();
			int deltax=Math.abs(this.listebonus.get(i).getCoordx()-this.Ship.getCoordx());//diff�rence d'abscisse entre le vaisseau et le bonus
			int deltay=Math.abs(this.listebonus.get(i).getCoordy()-this.Ship.getCoordy());//diff�rence d'ordonn�e entre le vaisseau et le bonus
			int dist=(int) Math.sqrt((deltax*deltax)+(deltay*deltay));//distance entre un bonus et le vaisseau
			if(dist<=Ship.longueur/2-this.listebonus.get(i).longueur/2){ //cas o� la collision � lieu
				switch (this.listebonus.get(i).getType()) {
				case "R":
					if(this.Ship.getPointdevie()<15){
						this.Ship.degats(-5);
					}
					else{
						this.Ship.setPointdevie(20);
					}

					break;
				case "C":
					for(int j=10;j<600;j=j+10){
						this.listetir.add(new Tir(j,520+j%20,0));
					}
					break;

				}
				this.listebonus.remove(i);
			}
		}
		repaint();
	}

	/**
	 * On paint avec cette methode tous les composants qui sont dans ce JPanel
	 * On peut dessiner presque tout ce qu'on veut. Il y a aussi l'affichage des images.
	 * @param n 
	 */
	@Override
	public void paintComponent(Graphics n){

		super.paintComponent(n);
		// Chargement de l'image @ adresse fond0.jpg
		Image img=(new javax.swing.ImageIcon(getClass().getResource("fond0.jpg"))).getImage();
		n.drawImage(img, 0, 0, null);

		n.setColor(Color.cyan);

		for(int i=0;i<this.listetir.size();i++){	//dessin des tirs
			Tir tircourant=listetir.get(i);
			if(tircourant.getAngle()==-1){//tirs � -45 degr�s
				int [] x1={tircourant.getCoordx(),tircourant.getCoordx(),tircourant.getCoordx()+5,tircourant.getCoordx()+15,tircourant.getCoordx()+10};
				int [] y1={tircourant.getCoordy()+5,tircourant.getCoordy(),tircourant.getCoordy(),tircourant.getCoordy()+10,tircourant.getCoordy()+15};
				n.fillPolygon(x1,y1, 5);
			}
			else{
				if(tircourant.getAngle()==1){//tirs � 45 degr�s
					int [] x2={tircourant.getCoordx()-5,tircourant.getCoordx(),tircourant.getCoordx(),tircourant.getCoordx()-10,tircourant.getCoordx()-15};
					int [] y2={tircourant.getCoordy(),tircourant.getCoordy(),tircourant.getCoordy()+5,tircourant.getCoordy()+15,tircourant.getCoordy()+10};
					n.fillPolygon(x2,y2, 5);
				}
				else{//tirs verticaux
					int [] x3={tircourant.getCoordx()-3,tircourant.getCoordx(),tircourant.getCoordx()+3,tircourant.getCoordx()+3,tircourant.getCoordx()-3};
					int [] y3={tircourant.getCoordy()+3,tircourant.getCoordy(),tircourant.getCoordy()+3,tircourant.getCoordy()+15,tircourant.getCoordy()+15};
					n.fillPolygon(x3,y3, 5);
				}
			}
		}

		n.setColor(Color.orange);
		for(int i=0;i<this.listaliens.size();i++){//dessin des aliens
			Alien a=this.listaliens.get(i);
			Image imgalien=(new javax.swing.ImageIcon(getClass().getResource("alien4.png"))).getImage();
			n.drawImage(imgalien,a.coordx-15, a.coordy-15, null);
		}

		n.setColor(Color.red);
		for(int i=0;i<this.listebombes.size();i++){		//dessin des bombes
			Bombe b=this.listebombes.get(i);
			n.fillOval(b.getCoordx()-b.taille/2,b.getCoordy()-b.taille/2, b.taille, b.taille);
		}


		if(this.niveau!=(-1)){
			n.setColor(Color.BLUE);		//dessin du vaisseau
			n.fillOval(this.Ship.coordx-this.Ship.longueur/2,this.Ship.coordy-this.Ship.hauteur/2,this.Ship.longueur,this.Ship.hauteur);
			Image imgvaisseau=(new javax.swing.ImageIcon(getClass().getResource("vaisseaupetit2.png"))).getImage();
			n.drawImage(imgvaisseau,this.Ship.coordx-29, this.Ship.coordy-30, null);

		}
		if(this.Ship.getPointdevie()>10){	//affichage barre de vie
			n.setColor(Color.GREEN);
		}
		else{
			if(this.Ship.getPointdevie()>5){
				n.setColor(Color.ORANGE);
			}
			else{
				n.setColor(Color.RED);
			}
		}
		n.fillRect(390, 620, this.Ship.getPointdevie()*10, 10); //barre de vie

		n.setColor(Color.white);
		if(niveau!=-1){
			n.drawString("SCORE = "+this.score, 25, 630);	//affichage du score
		}
		n.setColor(Color.WHITE);	//cadre autour de la barre de vie
		n.drawRect(390, 620, 200, 10);

		n.setColor(Color.white);
		if(this.niveau==-1){
			n.drawString("PERDU !!! SCORE = "+this.score, 250, 300);	//ecran de d�faite
			Image imgexplosion=(new javax.swing.ImageIcon(getClass().getResource("explosion.png"))).getImage();
			n.drawImage(imgexplosion,this.Ship.coordx-29, this.Ship.coordy-30, null);
		}
		for(int i=0;i<listebonus.size();i++){	//affichage des bonus
			Bonus b=this.listebonus.get(i);
			n.drawString(b.getType(), b.getCoordx()+2-b.longueur/2, b.getCoordy()-1+b.hauteur/2);
			n.drawRect(b.getCoordx()-b.longueur/2, b.getCoordy()-b.hauteur/2, b.longueur, b.hauteur);
		}
		if(this.niveau>=0){	//affichage du niveau (25=niveau MAX)

			if(this.niveau>=25){
				n.drawString("NIVEAU MAX", 25, 615);
			}
			else{
				n.drawString("NIVEAU = "+this.niveau, 25, 615);
			}
		}


	}



}