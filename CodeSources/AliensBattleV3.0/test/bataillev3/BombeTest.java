/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillev3;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author admin
 */
public class BombeTest {
    
    public BombeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of mouvement method, of class Bombe.
     */
    @Test
    public void testMouvement() {
        System.out.println("mouvement");
        Bombe instance = new Bombe(40,70,100,50);
        instance.mouvement();
        int xExpected=40+(int)((instance.dx/40)*instance.speed),yExpected=70+(int)((instance.dy/40)*instance.speed);
        assertEquals(yExpected, instance.getCoordy());
        assertEquals(xExpected, instance.getCoordx());
        
    }

    /**
     * Test of getCoordx method, of class Bombe.
     */
    @Test
    public void testGetCoordx() {
        System.out.println("getCoordx");
        Bombe instance =new Bombe(20,130,104,500);
        int expResult =20;
        int result = instance.getCoordx();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setCoordx method, of class Bombe.
     */
    @Test
    public void testSetCoordx() {
        System.out.println("setCoordx");
        int coordx = 50;
        Bombe instance = new Bombe(50,20,80,600);
        instance.setCoordx(coordx);
        assertEquals(coordx,instance.getCoordx());
    }

    /**
     * Test of getCoordy method, of class Bombe.
     */
    @Test
    public void testGetCoordy() {
        System.out.println("getCoordy");
        Bombe instance = new Bombe(50,40,200,80);
        int expResult =40;
        int result = instance.getCoordy();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setCoordy method, of class Bombe.
     */
    @Test
    public void testSetCoordy() {
        System.out.println("setCoordy");
        int coordy = 50;
        Bombe instance = new Bombe(80,50,300,10);
        instance.setCoordy(coordy);
        assertEquals(coordy, instance.getCoordy());
    }

    /**
     * Test of getDx method, of class Bombe.
     */
    @Test
    public void testGetDx() {
        System.out.println("getDx");
        Bombe instance =new Bombe(40,70,100,50);
        int expResult = instance.ciblex-instance.getCoordx();
        int result = instance.getDx();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setDx method, of class Bombe.
     */
    @Test
    public void testSetDx() {
        System.out.println("setDx");
        int dx = 10;
        Bombe instance = new Bombe(40,87,200,78);
        instance.setDx(dx);
        assertEquals(dx, instance.getDx());
    }

    /**
     * Test of getDy method, of class Bombe.
     */
    @Test
    public void testGetDy() {
        System.out.println("getDy");
        Bombe instance = new Bombe(50,80,401,55);
        int expResult = instance.cibley-instance.getCoordy();
        int result = instance.getDy();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setDy method, of class Bombe.
     */
    @Test
    public void testSetDy() {
        System.out.println("setDy");
        int dy = 10;
        Bombe instance = new Bombe(60,50,501,70);
        instance.setDy(dy);
        assertEquals(dy, instance.getDy());
    }
    
}
