/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bataillev3;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author admin
 */
public class BonusTest {
    
    public BonusTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getLongueur method, of class Bonus.
     */
    @Test
    public void testGetLongueur() {
        System.out.println("getLongueur");
        Bonus instance =new Bonus(40,20);
        instance.longueur=20;
        int expResult = 20;
        int result = instance.getLongueur();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getHauteur method, of class Bonus.
     */
    @Test
    public void testGetHauteur() {
        System.out.println("getHauteur");
        Bonus instance = new Bonus(40,20);
        instance.hauteur=10;
        int expResult = 10;
        int result = instance.getHauteur();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getType method, of class Bonus.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        Bonus instance = new Bonus(80,20);;
        String expResult = "R";
        instance.setType(expResult);
        String result = instance.getType();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setType method, of class Bonus.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        String type = "R";
        Bonus instance =new Bonus(40,20);;
        instance.setType(type);
        assertEquals(type, instance.getType());
    }

    /**
     * Test of getCoordy method, of class Bonus.
     */
    @Test
    public void testGetCoordy() {
        System.out.println("getCoordy");
        Bonus instance = new Bonus(40,20);
        int expResult = 20;
        int result = instance.getCoordy();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setCoordy method, of class Bonus.
     */
    @Test
    public void testSetCoordy() {
        System.out.println("setCoordy");
        int coordy = 10;
        Bonus instance = new Bonus(40,20);
        instance.setCoordy(coordy);
        assertEquals(coordy, instance.getCoordy());
    }

    /**
     * Test of trajectoire method, of class Bonus.
     */
    @Test
    public void testTrajectoire() {
        System.out.println("trajectoire");
        Bonus instance = new Bonus(40,20);
        int expecty= instance.getCoordy()+3;
        instance.trajectoire();
        assertEquals(expecty, instance.getCoordy());
        
       
    }

    /**
     * Test of getCoordx method, of class Bonus.
     */
    @Test
    public void testGetCoordx() {
        System.out.println("getCoordx");
        Bonus instance =new Bonus(40,20);
        int expResult = 40;
        int result = instance.getCoordx();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setCoordx method, of class Bonus.
     */
    @Test
    public void testSetCoordx() {
        System.out.println("setCoordx");
        int coordx = 50;
        Bonus instance = new Bonus(40,20);;
        instance.setCoordx(coordx);
        assertEquals(coordx, instance.getCoordx());
        
    }

    /**
     * Test of descendre method, of class Bonus.
     */
    @Test
    public void testDescendre() {
        System.out.println("descendre");
        int dy =10;
        Bonus instance =new Bonus(40,20);
        int expY= instance.getCoordy()-dy;
        instance.descendre(dy);
        assertEquals(expY, instance.getCoordy());
        
    }
    
}
