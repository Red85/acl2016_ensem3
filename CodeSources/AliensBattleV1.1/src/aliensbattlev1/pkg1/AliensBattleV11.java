/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aliensbattlev1.pkg1;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;

/**
 * @author acl2016_ensem3
 * @since  2016-11-10
 */
public class AliensBattleV11 {

    /**
     * @param args the command line arguments
     */
     /**
     * @param args 
     */
    public static void main(String[] args) {
        // Dimension du jeu
        Dimension dimension =new Dimension(Mouvant.Xmax,Mouvant.Ymax);
        Dimension d=new Dimension(Mouvant.Xmax/2,Mouvant.Ymax/2);// C'est pour le vaisseau!
        /** Je le met au centre à volonté, je pouvais bien fixer son ordonnée.
         *C'est pour faire un code flexible et réutilisable dans d'autres cas. 
         */
        
        //Parametrisation de la fenetre d jeu
        JFrame Fenetre= new JFrame();
        String title="AliensBattle";// Le titre
        Fenetre.setTitle(title);
        Fenetre.setSize(dimension);
        Fenetre.setResizable(false);// Pour empecher d'agrandir l'écran du jeu
        Fenetre.setLocationRelativeTo(null);//Pour le placer au milieu de l'écran
        Component vaisseau;
        vaisseau = new Mouvant(d);
        vaisseau.repaint();
        vaisseau.setForeground(Color.ORANGE);
        vaisseau.setPreferredSize(dimension);
        WindowAdapter wa= new WindowAdapter() {
            /**
             * 
             * @param e 
             */
            @Override
            public void windowClosing(WindowEvent e){
                System.exit(0);}};
        
        Fenetre.addWindowListener(wa);
        Fenetre.getContentPane().add(vaisseau);
        Fenetre.pack();
        Fenetre.setVisible(true);
        
    
}
    
}
