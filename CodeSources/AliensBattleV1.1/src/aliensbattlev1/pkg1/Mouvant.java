/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aliensbattlev1.pkg1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * @author acl2016_ensem3
 * @since  2016-11-10
 */

public class Mouvant extends JPanel {
     
//On fixe la taille de la fenetre du jeu avec ces paramètres
public static int Xmax=600, Ymax=650;
private int rayon;
private Dimension dimension;
/**
*@param D
* On construit un objet qui peut se deplacer suivant les instructions sur le clavier
*Dimension est un objet natif de Java, vous pouvez le voir le javadoc d'oracle
* si ça vous voulez.
*/
public Mouvant(Dimension D){
            
           /**
            * C'est pour le vaisseau!
            *Je le met à une coordonnée quelconque sans tenir compte du fait qu'il doit être posé sur le pa
            * le plateau, je pouvais bien fixer son ordonnée.
            * C'est pour faire un code flexible et réutilisable dans d'autres cas.
            * /
            
           / * 
            * ajout d'un écouteur d'évenements à partir du clavier.
            * On utilise ici uniquement les touches fléchées.
            */
            this.dimension=D;
            this.setFocusable(true);
            this.setBackground(Color.red);
            this.addKeyListener(new KeyListener()
                   /**
                    * Pour varier les postions dans la fénêtre.
                    * Comme vous l'aurez supposer, à chaque deplacement on doit redessiner,
                    * Donc à chaque fois qu'on change de position, il faut forcement redessiner.
                    * @param e evenement du clavier enum KeyEvent{ToucherLeClavier,RelacherLeClavier,}
                    */
             {   @Override
                public void keyPressed(KeyEvent e) {
                   
                   rayon=30;
                   int keyCode = e.getKeyCode();
                   switch( keyCode ) {
                       /**Touche flechée haut, on deplace le vaisseau vers le haut jusqu'à une certaine distance de 
                        * la bordure où le vaisseau n'est plus visible dans l'interface GUI
                        */
                       
                    case KeyEvent.VK_UP:
                            if(dimension.height>10 )
                    { 
                              dimension.height=dimension.height-5;
                                                       }
                            repaint();
                            break;
                      /**Touche flechée bas, on deplace le vaisseau vers le bas jusqu'à une certaine distance de la 
                       *bordure où le vaisseau n'est plus visible dans l'interface GUI
                       */       
                     case KeyEvent.VK_DOWN:
                            if(dimension.height<Ymax-50)
                            { 
                              dimension.height=dimension.height+5;
                             
                              }
                            repaint();
                            break;
                       /**Touche flechée gauche, on deplace le vaisseau vers la gauche jusqu'à une certaine distance
                        * de la bordure où où le vaisseau n'est plus visible dans l'interface GUI
                        */
                    case KeyEvent.VK_LEFT:
                              if(dimension.width>10)
                    { 
                              dimension.width = dimension.width -5;
                              
                              }
                               
                             repaint();
                            break;
                        /**Touche flechée droite, on deplace le vaisseau vers la droite jusqu'à une certaine distance 
                         *de la bordure où le vaisseau n'est plus visible dans l'interface GUI
                         */
                    case KeyEvent.VK_RIGHT :
                            if(dimension.width<Xmax-50)
                               
                            {dimension.width= dimension.width +5;
                          }
                             repaint();
                              break;
                     }
                     
                }
                /**
                 * On ne fait rien ici, les méthodes sont là parce que j'ai implementer
                 * l'interface.
                 * Heureusement les methodes ne retourne rien, donc je n'ai rien à ecrire
                 * dans ces deux fonctions si elles ne me sont pas utiles.
                 * @param e 
                 */
                @Override
                public void keyReleased(KeyEvent e) {}
                /**
                 * On ne fait rien ici, les méthodes sont là parce que j'ai implementer
                 * l'interface.
                 * Heureusement les methodes ne retourne rien, donc je n'ai rien à ecrire
                 * dans ces deux fonctions si elles ne me sont pas utiles.
                 * @param e 
                 */
                @Override
                public void keyTyped(KeyEvent e) {}
                 
                 
             });
                 
             
        }
/**
 * On paint avec cette methode tous les composants qui sont dans ce JPanel
 * On peut dessiner presque tout ce qu'on veut. Il y a aussi l'affichage des images.
 * @param n 
 */
@Override
public void paintComponent(Graphics n){

    super.paintComponent(n);
    // Chargement de l'image @ adresse fond0.jpg
    Image img=(new javax.swing.ImageIcon(getClass().getResource("fond0.jpg"))).getImage();
    n.drawImage(img, 0, 0, null);
    /** Dessin d'une ellipse de grand axe et petit axe this.rayon, en fait
     * c'est tout simplement un cercle!
     */
    n.fillOval(dimension.width, dimension.height, this.rayon, this.rayon);
   }

}
