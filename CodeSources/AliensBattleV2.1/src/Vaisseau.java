
public class Vaisseau {
	
	public int coordx;
	public int coordy;
	public int pointdevie=20;
	public double speed=1;
	
	public Vaisseau(int x,int y){
		this.coordx=x;
		this.coordy=y;
		
	}
	
	public void setspeed(int sp){
		speed=sp;
	}
	
//	public void deplacervertical(int dy){
//		coordy=coordy+dy;
//	}
	
	public void deplacerhorizontal(int dx){
		coordx=coordx+(int)(dx*speed);
	}
	
	public int getCoordx() {
		return coordx;
	}

	public void setCoordx(int coordx) {
		this.coordx = coordx;
	}

	public int getCoordy() {
		return coordy;
	}

	public void setCoordy(int coordy) {
		this.coordy = coordy;
	}

	public int getPointdevie() {
		return pointdevie;
	}

	public void setPointdevie(int pointdevie) {
		this.pointdevie = pointdevie;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public void degats(int nb){
		pointdevie=pointdevie-nb;
	}
	
	

}
