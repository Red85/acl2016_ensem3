import javax.swing.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;
import java.util.TimerTask;
import java.util.Vector;
import java.util.Timer;

/**
 * @author acl2016_ensem3
 * @since  2016-11-10
 */

public class Dessins extends JPanel {

	//On fixe la taille de la fenetre du jeu avec ces param�tres
	public static int Xmax=600, Ymax=650;
	private int score;
	private int niveau;
	public Vaisseau Ship=new Vaisseau(Xmax/2,550);
	public Vector<Alien> listaliens=new Vector();
	public Vector<Bombe> listebombes=new Vector();
	public Vector<Tir> listetir=new Vector();
	public Timer timertirs;
	public Timer timerappaliens;
	public Timer timerdeplaliens;
	/**
	 *@param D
	 * On construit un objet qui peut se deplacer suivant les instructions sur le clavier
	 *Dimension est un objet natif de Java, vous pouvez le voir le javadoc d'oracle
	 * si �a vous voulez.
	 */
	public Dessins(){

		/**
		 * C'est pour le vaisseau!
		 *Je le met � une coordonn�e quelconque sans tenir compte du fait qu'il doit �tre pos� sur le pa
		 * le plateau, je pouvais bien fixer son ordonn�e.
		 * C'est pour faire un code flexible et r�utilisable dans d'autres cas.
		 * /

           / * 
		 * ajout d'un �couteur d'�venements � partir du clavier.
		 * On utilise ici uniquement les touches fl�ch�es.
		 */
		//this.dimension=D;
		this.setFocusable(true);
		this.setBackground(Color.lightGray);






		this.addKeyListener(new KeyListener()
				/**
				 * Pour varier les postions dans la f�n�tre.
				 * Comme vous l'aurez supposer, � chaque deplacement on doit redessiner,
				 * Donc � chaque fois qu'on change de position, il faut forcement redessiner.
				 * @param e evenement du clavier enum KeyEvent{ToucherLeClavier,RelacherLeClavier,}
				 */
				{   public void keyPressed(KeyEvent e) {


					int keyCode = e.getKeyCode();
					switch( keyCode ) {


					/**Touche flech�e gauche, on deplace le vaisseau vers la gauche jusqu'� une certaine distance
					 * de la bordure o� o� le vaisseau n'est plus visible dans l'interface GUI
					 */
					case KeyEvent.VK_LEFT:

						if(Ship.getCoordx()>10){
							Ship.deplacerhorizontal(-10);
						}

						repaint();
						break;
						/**Touche flech�e droite, on deplace le vaisseau vers la droite jusqu'� une certaine distance 
						 *de la bordure o� le vaisseau n'est plus visible dans l'interface GUI
						 */
					case KeyEvent.VK_RIGHT :

						if(Ship.getCoordx()<Xmax-50){
							Ship.deplacerhorizontal(10);
						}
						repaint();
						break;


					case KeyEvent.VK_A :
						listetir.add(new Tir(Ship.getCoordx()-20,530,-1));
						repaint();
						break;

					case KeyEvent.VK_Z :
						listetir.add(new Tir(Ship.getCoordx()+1,520,0));
						repaint();
						break;

					case KeyEvent.VK_E :
						listetir.add(new Tir(Ship.getCoordx()+20,530,1));
						repaint();
						break;
					}

				}
				/**
				 * On ne fait rien ici, les m�thodes sont l� parce que j'ai implementer
				 * l'interface.
				 * Heureusement les methodes ne retourne rien, donc je n'ai rien � ecrire
				 * dans ces deux fonctions si elles ne me sont pas utiles.
				 * @param e 
				 */
				public void keyReleased(KeyEvent e) {}
				/**
				 * On ne fait rien ici, les m�thodes sont l� parce que j'ai implementer
				 * l'interface.
				 * Heureusement les methodes ne retourne rien, donc je n'ai rien � ecrire
				 * dans ces deux fonctions si elles ne me sont pas utiles.
				 * @param e 
				 */
				public void keyTyped(KeyEvent e) {}


				});

		timertirs = new Timer(); //movement des tirs
		timertirs.scheduleAtFixedRate(
				new TimerTask() 
				{	public void run()  { 
					actionperiodique1();
					try {
						actionperiodique4();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				}, 
				0, 250);

		timerappaliens = new Timer();  //apparition des aliens
		timerappaliens.scheduleAtFixedRate(
				new TimerTask() 
				{	public void run()  { 
					actionperiodique2();


				}
				}, 
				0, 3000);

		timerdeplaliens = new Timer(); //deplacement des aliens
		timerdeplaliens.scheduleAtFixedRate(
				new TimerTask() 
				{	public void run()  { 
					try {
						actionperiodique3();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				}, 
				200, 170);


	}

	public void actionperiodique1(){	//mouvement des tirs
		for(int i=0;i<this.listetir.size();i++){
			this.listetir.get(i).mouvement();
			if(listetir.get(i).getCoordy()<0){ //on enleve les tirs qui sont sortis de l'�cran
				listetir.remove(i);
			}
		}

		repaint();
		for(int i=0;i<this.listaliens.size()-1;i++){	//pour d�terminer la collision ou non d'un alien avec un tir
			for(int j=0;j<this.listetir.size();j++){
				int deltax=(this.listaliens.get(i).getCoordx()-this.listetir.get(j).getCoordx());//diff�rence d'abscisse entre le vaisseau et l'alien
				int deltay=(this.listaliens.get(i).getCoordy()-this.listetir.get(j).getCoordy());//diff�rence d'ordonn�e entre le vaisseau et l'alien
				int dist=Math.abs((deltax*deltax)-(deltay*deltay));//distance entre un alien et le vaisseau
				if(dist<=100){  	//cas o� un tir touche un alien
					this.score+=1;
					//System.out.println("Score : "+score);
					this.listaliens.remove(i);
					this.listetir.remove(j);
				}
			}
		}

	}

	public void actionperiodique2(){	//apparition des aliens
		if(this.niveau!=-1){
			int r=(int)(Math.random()*550);
			this.listaliens.add(new Alien(r));

			repaint();
		}
	}

	public void actionperiodique3() throws InterruptedException{	//deplacement des aliens (+ cr�ation des bombes)
		for(int i=0;i<this.listaliens.size();i++){
			//			this.listaliens.get(i).trajectoire(false);
			this.listaliens.get(i).trajectoireintel(this.Ship.coordx);
			if(listaliens.get(i).getCoordy()>700){
				listaliens.remove(i);
			}
		}

		for(int i=0;i<this.listaliens.size()-1;i++){	//pour d�terminer la collision ou non d'un alien avec le vaisseau

			int deltax=(this.listaliens.get(i).getCoordx()-this.Ship.getCoordx());//diff�rence d'abscisse entre le vaisseau et l'alien
			int deltay=(this.listaliens.get(i).getCoordy()-this.Ship.getCoordy());//diff�rence d'ordonn�e entre le vaisseau et l'alien
			int dist=Math.abs((deltax*deltax)-(deltay*deltay));//distance entre un alien et le vaisseau
			if(dist<=50){ //cas o� la collision � lieu
				this.Ship.setPointdevie(0);
				//System.out.println("Perdu");
				this.niveau=-1;
				Thread.sleep(1000000);
			}
		}
		double rdtir=Math.random(); //probabilit� de lancement de bombe d'un alien
		if(rdtir>0.85){
			int rd=(int) (Math.random()*(listaliens.size()-1)); //choix de l'alien qui va lancer la bombe
			listebombes.add(new Bombe(this.listaliens.get(rd).getCoordx(),this.listaliens.get(rd).getCoordy(),this.Ship.getCoordx(),this.Ship.getCoordy()));
			//System.out.println("Bombe largu�e par l'alien numero "+rd+" aux coordonn�es x="+this.listebombes.get(listebombes.size()-1).getCoordx()+" et y="+this.listebombes.get(listebombes.size()-1).getCoordy());
		}
		repaint();
	}

	public void actionperiodique4() throws InterruptedException{	//mouvement des bombes
		for(int i=0;i<this.listebombes.size();i++){
			this.listebombes.get(i).mouvement();
			if(this.listebombes.get(i).getCoordy()>700){//on enleve les bombes qui sont sorties de l'�cran
				this.listebombes.remove(i);
			}
		}
		repaint();
		for(int i=0;i<this.listebombes.size()-1;i++){	//pour d�terminer la collision ou non d'un alien avec le vaisseau

			int deltax=(this.listebombes.get(i).getCoordx()-this.Ship.getCoordx());//diff�rence d'abscisse entre le vaisseau et l'alien
			int deltay=(this.listebombes.get(i).getCoordy()-this.Ship.getCoordy());//diff�rence d'ordonn�e entre le vaisseau et l'alien
			int dist=Math.abs((deltax*deltax)-(deltay*deltay));//distance entre un alien et le vaisseau
			if(dist<=100){ //cas o� la collision � lieu
				//System.out.println("PV perdu");
				this.Ship.degats(1);
				this.listebombes.remove(i);
			}
			if(this.Ship.getPointdevie()<=0){
				//System.out.println("Perdu (plus de pv)");
				this.niveau=-1;
				Thread.sleep(100000);
			}
		}

	}
	/**
	 * On paint avec cette methode tous les composants qui sont dans ce JPanel
	 * On peut dessiner presque tout ce qu'on veut. Il y a aussi l'affichage des images.
	 * @param n 
	 */
	@Override
	public void paintComponent(Graphics n){

		super.paintComponent(n);
		// Chargement de l'image @ adresse fond0.jpg
		Image img=(new javax.swing.ImageIcon(getClass().getResource("fond0.jpg"))).getImage();
		n.drawImage(img, 0, 0, null);
		/** Dessin d'une ellipse de grand axe et petit axe this.rayon, en fait
		 * c'est tout simplement un cercle!
		 */


		n.setColor(Color.GREEN);

		for(int i=0;i<this.listetir.size();i++){//dessin des tirs
			Tir tircourant=listetir.get(i);
			if(tircourant.getAngle()==-1){//tirs � -45 degr�s
				int [] x1={tircourant.getCoordx(),tircourant.getCoordx()+5,tircourant.getCoordx()+30,tircourant.getCoordx()+25};
				int [] y1={tircourant.getCoordy()+5,tircourant.getCoordy(),tircourant.getCoordy()+25,tircourant.getCoordy()+30};
				n.fillPolygon(x1,y1, 4);
			}
			else{
				if(tircourant.getAngle()==1){//tirs � 45 degr�s
					int [] x2={tircourant.getCoordx(),tircourant.getCoordx()+25,tircourant.getCoordx()+30,tircourant.getCoordx()+5};
					int [] y2={tircourant.getCoordy()+25,tircourant.getCoordy(),tircourant.getCoordy()+5,tircourant.getCoordy()+30};
					n.fillPolygon(x2,y2, 4);
				}
				else{//tirs verticaux
					n.fillRect(this.listetir.get(i).getCoordx()+10, this.listetir.get(i).getCoordy(), 7, 30);
				}
			}
			//n.fillOval(this.listetir.get(i).getCoordx()+10, this.listetir.get(i).getCoordy(), 8, 30);
		}

		n.setColor(Color.ORANGE);
		for(int i=0;i<this.listaliens.size();i++){//dessin des aliens
			n.drawOval(this.listaliens.get(i).getCoordx(), this.listaliens.get(i).getCoordy(), 30, 30);
		}

		n.setColor(Color.RED);
		for(int i=0;i<this.listebombes.size();i++){//dessin des bombes
			n.fillOval(this.listebombes.get(i).getCoordx()+10, this.listebombes.get(i).getCoordy(), 8, 8);
		}

		n.setColor(Color.BLUE);
		n.fillOval(this.Ship.coordx, this.Ship.coordy, 30, 30);//dessin du vaisseau

		n.setColor(Color.GREEN);
		n.fillRect(390, 620, this.Ship.getPointdevie()*10, 10); //barre de vie

//		n.setColor(Color.BLACK);
//		if(niveau!=-1){
//			n.drawString("SCORE = "+this.score, 25, 630);//affichage du score
//		}
		n.drawRect(390, 620, 200, 10);

//		if(this.niveau==-1){
//			n.drawString("PERDU !!! SCORE = "+this.score, 250, 300);//ecran de d�faire
//		}
	}



}