
public class Tir {
	
	public int coordx;
	public int coordy;
	public int speed;
	public int angle;
	
	public Tir(int x,int y,int ang){
		/*ang vaut -1 si le tir est tir� avec un angle de -45 degr�s
		 *ang vaut 0 si le tir est tir� verticalement
		 * ang vaut -1 si le tir est tir� avec un angle de 45 degr�s
		 */
		this.coordx=x;
		this.coordy=y;
		this.angle=ang;
		
	}

	public int getCoordx() {
		return coordx;
	}

	public void setCoordx(int coordx) {
		this.coordx = coordx;
	}

	public int getCoordy() {
		return coordy;
	}

	public void setCoordy(int coordy) {
		this.coordy = coordy;
	}
	
	public void mouvement(){
		if(this.coordx>550||this.coordx<30){
			this.angle=this.angle*(-1);
		}
		this.coordy=this.coordy-15;
		this.coordx=this.coordx+15*this.angle;
	}

	public int getAngle() {
		return angle;
	}

	public void setAngle(int angle) {
		this.angle = angle;
	}
	

}
