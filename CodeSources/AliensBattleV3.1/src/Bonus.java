import java.util.Vector;

public class Bonus {

	private int longueur=12;
	private int hauteur=12;
	private int coordx;
	private int coordy;
	private String type;

	public Bonus(int x,int y,String t){
		this.coordx=x;
		this.coordy=y;
		this.type=t;

	}
	
	public Bonus(int x, int y){
		this.coordx=x;
		this.coordy=y;
		String [] listbonus={"R","C","S","B"};
		int rdbonus=(int) Math.floor(Math.random()*listbonus.length);
		this.type=listbonus[rdbonus];
	}
	
	public int getLongueur() {
		return longueur;
	}

	public int getHauteur() {
		return hauteur;
	}
	
	

	public void setLongueur(int longueur) {
		this.longueur = longueur;
	}

	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getCoordy() {
		return coordy;
	}

	public void setCoordy(int coordy) {
		this.coordy = coordy;
	}

	public void trajectoire(){
			coordy=coordy+3;
	}

	public int getCoordx() {
		return coordx;
	}

	public void setCoordx(int coordx) {
		this.coordx = coordx;
	}

	public void descendre(int dy){
		coordy=coordy-dy;
	}
}
