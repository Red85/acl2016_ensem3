
public class Vaisseau {
	
	private String type="";
	private int longueur=60;
	private int hauteur=60;
	private int coordx;
	private int coordy;
	private int pointdevie=20;
	private double speed=1;
	private boolean bouclier=false;
	private int taillebouclier=90;
	private int viebouclier=20;
	
	public Vaisseau(int x,int y){
		this.coordx=x;
		this.coordy=y;
		
	}
	
	public int getLongueur() {
		return longueur;
	}

	public int getHauteur() {
		return hauteur;
	}

	public void setspeed(int sp){
		speed=sp;
	}
	
	public void deplacervertical(int dy){
		coordy=coordy+dy;
	}
	
	public void deplacerhorizontal(int dx){
		coordx=coordx+(int)(dx*speed);
	}
	
	public int getCoordx() {
		return coordx;
	}

	public void setCoordx(int coordx) {
		this.coordx = coordx;
	}

	public int getCoordy() {
		return coordy;
	}

	public void setCoordy(int coordy) {
		this.coordy = coordy;
	}

	public int getPointdevie() {
		return pointdevie;
	}

	public void setPointdevie(int pointdevie) {
		this.pointdevie = pointdevie;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public void degats(int nb){
		pointdevie=pointdevie-nb;
	}
	
	public void degatsbouclier(int nb){
		viebouclier=viebouclier-nb;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
		
	}
	
	public Boolean getBouclier() {
		return bouclier;
	}

	public void setBouclier(Boolean bouclier) {
		this.bouclier = bouclier;
	}

	public int getTaillebouclier() {
		return taillebouclier;
	}
	
	public int getViebouclier() {
		return viebouclier;
	}

	public void setViebouclier(int viebouclier) {
		this.viebouclier = viebouclier;
	}
	
	

}
