
public class Alien {

	
	private int longueur=30;
	private int hauteur=30;
	private int coordx;
	private int coordy;
	private double speed=1;

	public Alien(int x){
		this.coordx=x;
		this.coordy=hauteur/2-200;	//en dehors de l'ecran pour eviter les aliens invincibles

	}
	
	public Alien(int x,double s){
		this.coordx=x;
		this.coordy=hauteur/2-200;	//en dehors de l'ecran pour eviter les aliens invincibles
		this.speed=s;

	}
	
	public int getLongueur() {
		return longueur;
	}

	public int getHauteur() {
		return hauteur;
	}

	public int getCoordy() {
		return coordy;
	}

	public void setCoordy(int coordy) {
		this.coordy = coordy;
	}

	public void trajectoireintel(int coordshipx){
		double rd=Math.random();//probabilité de déplacement intelligent (selon x)
		if(rd>0.75){
			coordx=coordx+(int)(4*(Math.signum(coordshipx-this.coordx)*this.speed));
		}
		if(rd<0.5){ //prob1;
		}
		coordy=coordy+(int)(4*this.speed);

	}

	public int getCoordx() {
		return coordx;
	}

	public void setCoordx(int coordx) {
		this.coordx = coordx;
	}

	public void descendre(int dy){
		coordy=coordy-dy;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

}
