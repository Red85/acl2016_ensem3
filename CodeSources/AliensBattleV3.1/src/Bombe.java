
public class Bombe {
	
	private int taille=8;
	private int coordx,coordy,ciblex,cibley,originex,originey;
	private int dx,dy;
	private double speed=1;
	
	public Bombe(int ox,int oy,int cx,int cy){
		coordx=ox;
		coordy=oy;
		originex=ox;
		originey=oy;
		ciblex=cx;
		cibley=cy;
		dx=cx-ox;
		dy=cy-oy;
	}
	
	public void mouvement(){
		int ajoutx=(int)((dx/100)*speed);
		int ajouty=(int)((dy/100)*speed);
		if(ajoutx==0 && ajouty==0){ //pour �viter les bombes immobiles
			coordx=1;
		}
		coordx=coordx+ajoutx;
		coordy=coordy+ajouty;
		
	}
	

	public int getTaille() {
		return taille;
	}

	public void setTaille(int taille) {
		this.taille = taille;
	}

	public int getCoordx() {
		return coordx;
	}

	public void setCoordx(int coordx) {
		this.coordx = coordx;
	}

	public int getCoordy() {
		return coordy;
	}

	public void setCoordy(int coordy) {
		this.coordy = coordy;
	}

	public int getDx() {
		return dx;
	}

	public void setDx(int dx) {
		this.dx = dx;
	}

	public int getDy() {
		return dy;
	}

	public void setDy(int dy) {
		this.dy = dy;
	}
	
	

}
