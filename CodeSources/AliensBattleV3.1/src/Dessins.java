import javax.swing.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.TimerTask;
import java.util.Vector;
import java.util.Timer;

/**
 * @author acl2016_ensem3
 * @since  2016-11-10
 */

public class Dessins extends JPanel {

	//On fixe la taille de la fenetre du jeu avec ces param�tres


	private static File f = new File ("tableauscore.txt");// fichier texte permettant de recup�rer les scores des diff�rentes parties
	private static BufferedReader fR; //pour r�cup�rer les meilleurs scores
	private static FileWriter fw; //pour �crire les meilleurs scores
	private static int Xmax=600;
	private static int Ymax=650;
	private int rdfond=(int)(Math.random()*6); //choisit l'image de fond du jeu(au hasard)
	private int score;
	private int[] tablscores;
	private int niveau;
	private double alienspeed=1;
	private int munition=20;
	private Vaisseau Ship=new Vaisseau(Xmax/2,Ymax-100);
	private Vector<Alien> listaliens=new Vector<Alien>();
	private Vector<Bombe> listebombes=new Vector<Bombe>();
	private Vector<Tir> listetir=new Vector<Tir>();
	private Timer timermunition;
	private Timer timertirs;
	private Timer timerappaliens;
	private Timer timerdeplaliens;
	private Timer timerniveau;
	private Vector<Bonus> listebonus=new Vector();
	private boolean pause=false;
	private boolean start=false;
	private boolean repet=false;

	public Dessins(){

		this.setFocusable(true);
		this.setBackground(Color.lightGray);
		this.addKeyListener(new KeyListener()
		{   public void keyPressed(KeyEvent e) {

			int keyCode = e.getKeyCode();
			switch( keyCode ) {

			/**Touche flech�e gauche, on deplace le vaisseau vers la gauche jusqu'� une certaine distance
			 * de la bordure o� o� le vaisseau n'est plus visible dans l'interface GUI
			 */
			case KeyEvent.VK_LEFT:
				if(pause==false&&start==true){
					if(Ship.getCoordx()>20&&niveau!=(-1)){
						Ship.deplacerhorizontal(-10);
					}

					repaint();
				}
				break;
				/**Touche flech�e droite, on deplace le vaisseau vers la droite jusqu'� une certaine distance 
				 *de la bordure o� le vaisseau n'est plus visible dans l'interface GUI
				 */
			case KeyEvent.VK_RIGHT :
				if(pause==false&&start==true){
					if(Ship.getCoordx()<Xmax-20&&niveau!=(-1)){
						Ship.deplacerhorizontal(10);
					}
					repaint();
				}
				break;

			case KeyEvent.VK_UP:
				if(pause==false&&start==true){
					if(Ship.getCoordy()>50&&niveau!=(-1)){
						Ship.deplacervertical(-10);
					}

					repaint();
				}
				break;

			case KeyEvent.VK_DOWN:
				if(pause==false&&start==true){
					if(Ship.getCoordy()<600&&niveau!=(-1)){
						Ship.deplacervertical(10);
					}

					repaint();
				}
				break;


			case KeyEvent.VK_A :  // touche de tir a 45� � gauche
				if(pause==false&&start==true){
					if(munition>0){
						if(niveau!=(-1)){
							listetir.add(new Tir(Ship.getCoordx()-20,Ship.getCoordy(),-1));
						}
						repaint();
						munition--;
					}
				}
				break;

			case KeyEvent.VK_Z : // touche de tir vertical
				if(pause==false&&start==true){
					if(munition>0){
						if(niveau!=(-1)){
							listetir.add(new Tir(Ship.getCoordx(),Ship.getCoordy(),0));
						}
						repaint();
						munition--;
					}
				}
				break;

			case KeyEvent.VK_E :  // touche de tir a 45� � droite
				if(pause==false&&start==true){
					if(munition>0){
						if(niveau!=(-1)){
							listetir.add(new Tir(Ship.getCoordx()+20,Ship.getCoordy(),1));
						}
						repaint();
						munition--;
					}
				}
				break;


				// les touches suivantes permettent de choisir son type de vaisseau
			case KeyEvent.VK_Y :
				if(start==false){
					Ship.setType("Y");
					start=true;
				}
				break;

			case KeyEvent.VK_U :
				if(start==false){
					Ship.setType("U");
					start=true;
				}
				break;

			case KeyEvent.VK_I :
				if(start==false){
					Ship.setType("I");
					start=true;
				}
				break;

			case KeyEvent.VK_O :
				if(start==false){
					Ship.setType("O");
					start=true;
				}
				break;

			case KeyEvent.VK_P :
				if(start==false){
					Ship.setType("P");
					start=true;
				}
				break;

				//touche qui sert � mettre en pause le jeu
			case KeyEvent.VK_SPACE :
				if(pause==true){
					pause=false;
				}
				else{
					pause=true;
				}
				repaint();
				break;
			}
		}

		public void keyReleased(KeyEvent e) {}
		public void keyTyped(KeyEvent e) {}
		});


		timerniveau = new Timer();	//gestion des niveau de difficult� (10secondes = +1 niv)
		timerniveau.scheduleAtFixedRate(
				new TimerTask()
				{
					public void run(){
						if(start==true&&pause==false){
							if(niveau<25&&niveau>=0){
								niveau++;
								alienspeed=alienspeed+0.1;
							}
						}
					}
				},1000,20000);

		timermunition = new Timer ();	//rechargement des munitions
		timermunition.scheduleAtFixedRate(
				new TimerTask() 
				{	public void run()  {
					if(munition<20){
						munition++;
					}
				}
				},
				0,1000);

		timertirs = new Timer(); 	//movement des tirs et bombes
		timertirs.scheduleAtFixedRate(
				new TimerTask() 
				{	public void run()  { 
					actionperiodique1();
					try {
						actionperiodique4();
					} catch (InterruptedException | IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				}, 
				0, 12);

		timerappaliens = new Timer();  //apparition des aliens
		timerappaliens.scheduleAtFixedRate(
				new TimerTask() 
				{	public void run()  { 
					actionperiodique2();
				}
				}, 

				0, 2500);

		timerdeplaliens = new Timer(); //deplacement des aliens
		timerdeplaliens.scheduleAtFixedRate(
				new TimerTask() 
				{	public void run()  { 
					actionperiodique5();
					try {
						actionperiodique3();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				}, 
				0, 150);
	}

	public void actionperiodique1(){	//mouvement des tirs
		if(pause==false&&start==true){
			for(int i=0;i<this.listetir.size();i++){
				this.listetir.get(i).mouvement();
				if(listetir.get(i).getCoordy()<0){ //on enleve les tirs qui sont sortis de l'�cran
					listetir.remove(i);
				}
			}

			repaint();
			for(int i=0;i<this.listaliens.size()-this.niveau;i++){	//pour d�terminer la collision ou non d'un alien avec un tir
				for(int j=0;j<this.listetir.size();j++){
					int deltax=Math.abs(this.listaliens.get(i).getCoordx()-this.listetir.get(j).getCoordx());//diff�rence d'abscisse entre le tir et l'alien
					int deltay=Math.abs(this.listaliens.get(i).getCoordy()-this.listetir.get(j).getCoordy());//diff�rence d'ordonn�e entre le tir et l'alien
					int dist=(int) Math.sqrt((deltax*deltax)+(deltay*deltay));//distance entre un alien et un tir
					if(dist<=this.listaliens.get(i).getLongueur()/2){  	//cas o� un tir touche un alien
						this.score+=1;
						double probbonus=Math.random();
						if(probbonus>0.8+(niveau*0.01)){
							this.listebonus.addElement(new Bonus(this.listaliens.get(i).getCoordx(),this.listaliens.get(i).getCoordy()));
						}
						this.listaliens.remove(i);
						this.listetir.remove(j);
						if(munition<20){//on gagne une munition lorsque un alien meurt
							this.munition++;
						}
					}
				}
			}
		}

	}

	public void actionperiodique2(){	//apparition des aliens
		if(pause==false&&start==true){

			int nbapp=(int)(Math.random()*(this.niveau))+1;
			for(int i=0;i<nbapp;i++){
				Alien a=new Alien(0,alienspeed);
				int r=(int)(Math.random()*(Xmax-a.getLongueur()/2));
				if(r<a.getLongueur()/2){
					r=a.getLongueur()/2;
				}
				this.listaliens.add(new Alien(r));
			}
			repaint();

		}
	}

	public void actionperiodique3() throws InterruptedException, IOException{	//deplacement des aliens (+ cr�ation des bombes)
		if(pause==false&&start==true){
			for(int i=0;i<this.listaliens.size();i++){
				this.listaliens.get(i).trajectoireintel(this.Ship.getCoordx());
				if(listaliens.get(i).getCoordy()>Ymax+20){
					listaliens.remove(i);
				}

			}

			for(int i=0;i<this.listaliens.size();i++){	//pour d�terminer la collision ou non d'un alien avec le vaisseau ou le bouclier
				int deltax=Math.abs(this.listaliens.get(i).getCoordx()-this.Ship.getCoordx());//diff�rence d'abscisse entre le vaisseau et l'alien
				int deltay=Math.abs(this.listaliens.get(i).getCoordy()-this.Ship.getCoordy());//diff�rence d'ordonn�e entre le vaisseau et l'alien
				int dist=(int) Math.sqrt((deltax*deltax)+(deltay*deltay));//distance entre un alien et le vaisseau
				if(Ship.getBouclier()){
					if(dist<Ship.getTaillebouclier()/2+listaliens.get(i).getLongueur()/2){ //cas o� la collision � lieu avec le bouclier
						this.listaliens.remove(i);
						Ship.degatsbouclier(15);
						if(Ship.getViebouclier()<=0){
							Ship.setBouclier(false);
						}
					}
				}
				else{
					if(dist<Ship.getLongueur()/2+listaliens.get(i).getLongueur()/2){ //cas o� la collision � lieu avec un alien
						this.Ship.setPointdevie(0);
						this.niveau=-1;
						this.listetir.removeAllElements();
						this.listebombes.removeAllElements();

					}
				}
			}
			double rdbombe=Math.random(); //probabilit� de lancement de bombe d'un alien
			if(this.niveau>0){
				if(rdbombe>0.95){
					int rd=(int) (Math.random()*(listaliens.size()-1)); //choix de l'alien qui va lancer la bombe
					if(listaliens.get(rd).getCoordy()>0){
						listebombes.add(new Bombe(this.listaliens.get(rd).getCoordx(),this.listaliens.get(rd).getCoordy(),this.Ship.getCoordx(),this.Ship.getCoordy()));
					}
				}
				repaint();
			}
		}
	}

	public void actionperiodique4() throws InterruptedException, IOException{	//mouvement des bombes
		if(pause==false&&start==true){
			for(int i=0;i<this.listebombes.size();i++){
				this.listebombes.get(i).mouvement();
				if(this.listebombes.get(i).getCoordy()>Ymax+20 || this.listebombes.get(i).getCoordy()<0){//on enleve les bombes qui sont sorties de l'�cran
					this.listebombes.remove(i);
				}
			}
			repaint();
			for(int i=0;i<this.listebombes.size();i++){	//pour d�terminer la collision ou non d'une bombe avec le vaisseau
				int deltax=Math.abs(this.listebombes.get(i).getCoordx()-this.Ship.getCoordx());//diff�rence d'abscisse entre le vaisseau et une bombe
				int deltay=Math.abs(this.listebombes.get(i).getCoordy()-this.Ship.getCoordy());//diff�rence d'ordonn�e entre le vaisseau et une bombe
				int dist=(int) Math.sqrt((deltax*deltax)+(deltay*deltay));//distance entre une bombe et le vaisseau
				if(Ship.getBouclier()){
					if(dist<Ship.getTaillebouclier()/2+listebombes.get(i).getTaille()/2){ //cas o� la collision � lieu avec le bouclier
						this.listebombes.remove(i);
						Ship.degatsbouclier(4);
						if(Ship.getViebouclier()<=0){
							Ship.setBouclier(false);
						}
					}
				}
				else{
					if(dist<=Ship.getLongueur()/2+this.listebombes.get(i).getTaille()/2){ //cas o� la collision � lieu
						this.Ship.degats(1);
						this.listebombes.remove(i);
					}
					if(this.Ship.getPointdevie()<=0){
						this.niveau=-1;
						this.listetir.removeAllElements();
						this.listebombes.removeAllElements();
					}
				}
			}
		}
	}

	public void actionperiodique5(){ //collisions du vaisseau avec les bonus
		if(pause==false&&start==true){
			for(int i=0;i<listebonus.size();i++){
				this.listebonus.get(i).trajectoire();
				int deltax=Math.abs(this.listebonus.get(i).getCoordx()-this.Ship.getCoordx());//diff�rence d'abscisse entre le vaisseau et le bonus
				int deltay=Math.abs(this.listebonus.get(i).getCoordy()-this.Ship.getCoordy());//diff�rence d'ordonn�e entre le vaisseau et le bonus
				int dist=(int) Math.sqrt((deltax*deltax)+(deltay*deltay));//distance entre un bonus et le vaisseau
				if(dist<=Ship.getLongueur()/2-this.listebonus.get(i).getLongueur()/2){ //cas o� la collision � lieu
					switch (this.listebonus.get(i).getType()) {

					case "R": // si c'est un bonus de r�paration
						if(this.Ship.getPointdevie()<15){
							this.Ship.degats(-5); // ajout de 5 points de vie
						}
						else{
							this.Ship.setPointdevie(20); 
						}

						break;
					case "C": // si c'est un bonus d'effacement d'alien (clear)
						for(int j=10;j<600;j=j+10){
							this.listetir.add(new Tir(j,520+j%20,0));
						}
						break;
					case "S": // si c'est un bonus de score
						this.score+=5;
						break;
					case "B":
						Ship.setBouclier(true);
						Ship.setViebouclier(20);
						break;
					}
					this.listebonus.remove(i);
				}
			}
			repaint();
		}
	}

	public int[] actualiserscores(int scoreactuel) throws IOException{ //utilise le score actuel 
		//et la liste des meilleurs scores pour cr�er un tableau de int contentant les 5 meilleurs scores
		BufferedReader fR=new BufferedReader(new FileReader(f));
		int[] bestscores=new int[5];
		Vector<Integer> meillscores=new Vector<Integer>();
		String str="";
		int compt=0;
		do{//cr�ation d'un tableau de int content le 5 meilleurs scores de notre File
			str = fR.readLine();
			meillscores.add(Integer.parseInt(str));
			compt++;
		}while(compt<5);
		fR.close();
		FileWriter fw = new FileWriter (f);
		meillscores.add(scoreactuel);
		meillscores.sort(null);
		for(int i=0;i<5;i++){
			bestscores[i]= meillscores.get(meillscores.size()-(i+1));
			fw.write (bestscores[i]+"\n");
		}
		fw.close();
		return bestscores;
	}
	


	public static int getXmax() {
		return Xmax;
	}

	public static void setXmax(int xmax) {
		Xmax = xmax;
	}

	public static int getYmax() {
		return Ymax;
	}

	public static void setYmax(int ymax) {
		Ymax = ymax;
	}

	@Override
	public void paintComponent(Graphics n){

		super.paintComponent(n);
		Image imgalien=(new javax.swing.ImageIcon(getClass().getResource("alien4.png"))).getImage();
		Image imgpause=(new javax.swing.ImageIcon(getClass().getResource("pause.png"))).getImage();
		Image gameover=(new javax.swing.ImageIcon(getClass().getResource("gameover.png"))).getImage();
		Image imgvaisseau=(new javax.swing.ImageIcon(getClass().getResource("vaisseau1grand.png"))).getImage();
		Image imgvaisseau3=(new javax.swing.ImageIcon(getClass().getResource("vaisseau3grand.png"))).getImage();
		Image imgvaisseau4=(new javax.swing.ImageIcon(getClass().getResource("vaisseau4grand.png"))).getImage();
		Image imgvaisseau5=(new javax.swing.ImageIcon(getClass().getResource("vaisseau5grand.png"))).getImage();
		Image imgvaisseau6=(new javax.swing.ImageIcon(getClass().getResource("vaisseau6grand.png"))).getImage();
		Image imgtitre=(new javax.swing.ImageIcon(getClass().getResource("titrejeu.png"))).getImage();
		Image img=(new javax.swing.ImageIcon(getClass().getResource("fond"+rdfond+".png"))).getImage();
		


		n.drawImage(img, 0, 0, null);
		Image imgvaisseauutil = null;

		switch(Ship.getType()){
		case "Y":
			imgvaisseauutil=imgvaisseau4;
			break;
		case "U":
			imgvaisseauutil=imgvaisseau5;
			break;
		case "I":
			imgvaisseauutil=imgvaisseau;
			break;
		case "O":
			imgvaisseauutil=imgvaisseau6;
			break;
		case "P":
			imgvaisseauutil=imgvaisseau3;
			break;
		}

		n.setColor(Color.cyan);

		for(int i=0;i<this.listetir.size();i++){//dessin des tirs
			Tir tircourant=listetir.get(i);
			if(tircourant.getAngle()==-1){//tirs � -45 degr�s
				int [] x1={tircourant.getCoordx(),tircourant.getCoordx(),tircourant.getCoordx()+5,tircourant.getCoordx()+15,tircourant.getCoordx()+10};
				int [] y1={tircourant.getCoordy()+5,tircourant.getCoordy(),tircourant.getCoordy(),tircourant.getCoordy()+10,tircourant.getCoordy()+15};
				n.fillPolygon(x1,y1, 5);
			}
			else{
				if(tircourant.getAngle()==1){//tirs � 45 degr�s
					int [] x2={tircourant.getCoordx()-5,tircourant.getCoordx(),tircourant.getCoordx(),tircourant.getCoordx()-10,tircourant.getCoordx()-15};
					int [] y2={tircourant.getCoordy(),tircourant.getCoordy(),tircourant.getCoordy()+5,tircourant.getCoordy()+15,tircourant.getCoordy()+10};
					n.fillPolygon(x2,y2, 5);
				}
				else{//tirs verticaux
					int [] x3={tircourant.getCoordx()-3,tircourant.getCoordx(),tircourant.getCoordx()+3,tircourant.getCoordx()+3,tircourant.getCoordx()-3};
					int [] y3={tircourant.getCoordy()+3,tircourant.getCoordy(),tircourant.getCoordy()+3,tircourant.getCoordy()+15,tircourant.getCoordy()+15};
					n.fillPolygon(x3,y3, 5);
				}
			}
		}

		for(int i=0;i<this.listaliens.size();i++){	//dessin des aliens
			Alien a=this.listaliens.get(i);
			n.drawImage(imgalien,a.getCoordx()-15, a.getCoordy()-15, null);
		}

		n.setColor(Color.red);
		for(int i=0;i<this.listebombes.size();i++){	//dessin des bombes
			Bombe b=this.listebombes.get(i);
			n.fillOval(b.getCoordx()-b.getTaille()/2,b.getCoordy()-b.getTaille()/2, b.getTaille(), b.getTaille());
		}

		if(this.niveau!=(-1)){	//dessin du vaisseau
			n.drawImage(imgvaisseauutil,this.Ship.getCoordx()-29, this.Ship.getCoordy()-30, null);
		}

		if(this.Ship.getPointdevie()>10){	//couleur de la barre de vie
			n.setColor(Color.GREEN);
		}
		else{
			if(this.Ship.getPointdevie()>5){
				n.setColor(Color.ORANGE);
			}
			else{
				n.setColor(Color.RED);
			}
		}

		n.fillRect(390, 620, this.Ship.getPointdevie()*10, 10); //affichage barre de vie
		n.setColor(Color.WHITE);	
		n.drawRect(390, 620, 200, 10);	//cadre autour de la barre de vie


		if(this.Ship.getViebouclier()>10){	//couleur du bouclier
			n.setColor(Color.GREEN);
		}
		else if(this.Ship.getViebouclier()>5){
			n.setColor(Color.ORANGE);
		}
		else if(this.Ship.getViebouclier()>1){
			n.setColor(Color.red);
		}
		else{
			n.setColor(Color.black);
		}

		if(Ship.getBouclier()){   //affichage du bouclier
			n.drawOval(Ship.getCoordx()-Ship.getTaillebouclier()/2, Ship.getCoordy()-Ship.getTaillebouclier()/2, Ship.getTaillebouclier(), Ship.getTaillebouclier());
		}

		n.setColor(Color.WHITE);
		if(niveau!=-1){
			n.drawString("SCORE = "+this.score, 25, 630);	//affichage du score
		}

		if(this.niveau==-1&&repet==false){ //cas de d�faite
			try {
				this.tablscores=actualiserscores(this.score);
				repet=true;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if(this.niveau==-1){ //cas de d�faite
			n.drawString("YOUR SCORE = "+this.score, 250, 400);
			Image imgexplosion=(new javax.swing.ImageIcon(getClass().getResource("explosion.png"))).getImage();
			n.drawImage(imgexplosion,this.Ship.getCoordx()-29, this.Ship.getCoordy()-30, null);
			n.drawImage(gameover,140, 275, null);

			String strscores="Best scores :"+"\n";
			for(int i=0;i<5;i++){
				strscores=strscores+"      "+this.tablscores[i]+"\n";
			}
			n.drawString(strscores,160, 460);
		}

		n.setColor(Color.white);
		for(int i=0;i<listebonus.size();i++){	//affichage des bonus
			n.drawString(this.listebonus.get(i).getType(), this.listebonus.get(i).getCoordx()+2, this.listebonus.get(i).getCoordy()+1);
			n.drawRect(this.listebonus.get(i).getCoordx(), this.listebonus.get(i).getCoordy()-10, this.listebonus.get(i).getLongueur(), this.listebonus.get(i).getHauteur());
		}

		if(this.niveau>=0){		//affichage du niveau (25=niveau MAX)
			if(this.niveau>=25){
				n.drawString("NIVEAU MAX", 25, 615);
			}
			else{
				n.drawString("NIVEAU = "+this.niveau, 25, 615);
			}
			n.drawString(this.munition+"/20", 390, 645);//affichage des munitions
		}

		if(pause==true){  //affichage en cas de pause
			n.drawImage(imgpause, 100, 200, null);
		}

		n.setColor(Color.WHITE);
		//affichage du d�but de la partie
		if(start==false){
			n.drawString("CHOOSE YOUR SHIP TO START !",220, 300);
			n.drawImage(imgtitre,70, 100, null);
			n.drawImage(imgvaisseau4,90, 375, null);
			n.drawString("Y", 115, 450);
			n.drawImage(imgvaisseau5,185, 375, null);
			n.drawString("U", 210, 450);
			n.drawImage(imgvaisseau,280, 375, null);
			n.drawString("I", 310, 450);
			n.drawImage(imgvaisseau6,385, 375, null);
			n.drawString("O", 410, 450);
			n.drawImage(imgvaisseau3,485, 375, null);
			n.drawString("P", 510, 450);
			n.drawRoundRect(70, 350, 500, 120, 30, 30);
		}
	}



}