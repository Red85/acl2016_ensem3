import java.awt.Graphics;
import java.util.Vector;

import javax.swing.JPanel;

public class Panneau extends JPanel { 

	private Vector vectcoords;
	private int cx;
	private int cy;
	
	public Panneau(Vector vect,int coordx, int coordy){
		this.vectcoords=vect;
		this.cx=coordx;
		this.cy=coordy;
		
	}

	public void paintComponent(Graphics g){		
		g.fillOval(((int)vectcoords.get(vectcoords.size()-1)*100)*(cx/1000),800, 50, 50);
		
	}
	
	public void setcoordx(int coord){
		vectcoords.add(coord);
	}
}